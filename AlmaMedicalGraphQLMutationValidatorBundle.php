<?php

namespace AlmaMedical\GraphQLMutationValidatorBundle;

use AlmaMedical\GraphQLMutationValidatorBundle\DependencyInjection\AlmaMedicalGraphQLMutationValidatorExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

Class AlmaMedicalGraphQLMutationValidatorBundle extends Bundle
{
}