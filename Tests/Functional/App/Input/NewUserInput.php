<?php

namespace AlmaMedical\GraphQLMutationValidatorBundle\Tests\Functional\App\Input;

use AlmaMedical\GraphQLMutationValidatorBundle\Input\RequestObject;
use AlmaMedical\GraphQLMutationValidatorBundle\Tests\Functional\App\Entity\User;
use AlmaMedical\GraphQLMutationValidatorBundle\Validator\Constraints as AlmaMedicalAssert;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @AlmaMedicalAssert\GraphQLRequestObject()
 */
Class NewUserInput extends RequestObject
{

    /**
     * @see User::$username
     */
    public $username;

    /**
     * @Assert\NotBlank()
     */
    public $firstname;

}